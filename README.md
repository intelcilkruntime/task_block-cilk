C++ task_block prototype using Cilk Plus runtime scheduler
==========================================================

The Clang tool in this directory is a source-to-source translator that allows
the `task_block` constructs defined in
[P0155R0](http://www.open-std.org/JTC1/SC22/WG21/docs/papers/2015/p0155r0.pdf) to
work using a continuation-stealing scheduler.  The `task_block` library is a
draft extension to C++ to provide fork-join parallelism.  This
proof-of-concept implementation is built on top of the Intel(R) Cilk(TM) Plus
runtime scheduler.

Although a child-stealing implementation of N4411 is possible with no compiler
support beyond standard C++11 features, continuation stealing provides a
number of advantages, including mathematically provable asymptotic memory
bounds and less expensive spawns. Unlike child stealing, compiler assistance
is required to get continuation stealing. In the process, the compiler can
perform a number of compile-time error checks that would not be possible using
a library-only implementation.  Specifically, using the wrong `task_block` to
launch a task is usually detectable at compile time.

Being a limited proof-of-concept, this implementation has a number of failings
that cause it to fall short of production quality, including:

   * Source-to-source translation is inherently clunky, requiring two
     invocations of the compiler.
   * If `task_block` is used within a header file (i.e., within an inline
     function or function template), those uses will not be correctly
     instrumented.  Only uses of `task_block` within the main `.cpp` file will
     be handled correctly.
   * If a `task_block` is used as an argument to instantiate a template, it
     will not get the special treatment it needs within the template
     instantiation.  For this reason, C++14 polymorphic lambdas cannot be used
     as the body of a `define_task_block` invocation, nor can an object of a
     functor class type where `operator()` is a template.
   * Parallel exception handling is not yet implemented.

Getting the source code
-----------------------

This repository contains only the source code for the **TaskBlock** tool, not
for the rest of Clang/llvm.  You will need to layer this directory tree on top
of the Clang/llvm tree in order to build. It is theoretically possible to build
this tool with a compiler that does not support Cilk Plus.  In that case,
however, you would need to obtain the Cilk Plus runtime library, including
internal header files, separately from
[cilkplus.org](https://www.cilkplus.org).  For simplicity, the instructions
below use the Cilk Plus fork of Clang/llvm, which includes the Cilk Plus
runtime library. These instructions are adapted from the
[Cilk Plus/LLVM](http://cilkplus.github.io/) web page.

    $ cd where-you-want-llvm-to-live
    $ git clone -b cilkplus https://github.com/cilkplus/cilkplus-llvm
    $ git clone -b cilkplus https://github.com/cilkplus/clang cilkplus-llvm/tools/clang
    $ git clone -b cilkplus https://github.com/cilkplus/compiler-rt cilkplus-llvm/projects/compiler-rt
    $ git clone git@bitbucket.org:intelcilkruntime/task_block-cilk.git cilkplus-llvm/tools/clang/examples/TaskBlock
    $ echo "add_subdirectory(TaskBlock)" >> tools/clang/examples/CMakeLists.txt 

Building
--------

Again, these instructions are adapted from the
[Cilk Plus/LLVM](http://cilkplus.github.io/) web page.  Assuming you put the
source code into a directory called `cilkplus-llvm`, you can build llvm,
clang, and the tool as follows (the initial build can easily take more than an
hour, but changes to the tool will not require a full rebuild).

    $ cd cilkplus-llvm
    $ mkdir build
    $ # THE FOLLOWING LINE IS A HACK TO GET AROUND A BROKEN BUILD CONFIG
    $ cp -p projects/compiler-rt/lib/cilk/runtime/config/x86/os-fence.h projects/compiler-rt/lib/cilk/runtime
    $ cd build
    $ # Note use of "Debug". Substitute "Release" if preferred.
    $ cmake -DCMAKE_INSTALL_PREFIX=. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS=-std=c++11 -DLLVM_TARGETS_TO_BUILD=X86 ..
    $ make
    $ cd build/tools/clang/examples/TaskBlock
    $ make

**Thereafter, if making changes to the tool, rebuild by just calling `make`:**

    $ cd build/tools/clang/examples/TaskBlock
    $ make

Using the tool
--------------

The syntax for writing C++ code using the `task_block` fork-join constructs is
described adequately in
[P0155R0](http://www.open-std.org/JTC1/SC22/WG21/docs/papers/2015/p0155r0.pdf) and
will not be repeated here.

The best way to get started is to build the test program in
cilkplus-llvm/tools/clang/examples/TaskBlock/test. The `Makefile` is set up to
build within the directory structure above. You can modify it as needed for
other directory structures or other programs.

Consider the following instructions as a rough guide. Use the test program
`Makefile` as a more definitive source.

Set up abbreviations for important directories:

    $ TASKBLOCK_TOOLSRC_DIR=dir-where-TaskBlock-was-cloned
    $ LLVM_BUILD_DIR=root-of-llvm-build

Generate *sourcefile*`_gen.cpp` from *sourcefile*`.cpp`:

    $ $LLVM_BUILD_DIR/bin/clang -std=c++11 -I $TASKBLOCK_TOOLSRC_DIR/include \
      -Xclang -load -Xclang $LLVM_BUILD_DIR/lib/TaskBlock.so -Xclang \
      -add-plugin -Xclang TaskBlock -emit-llvm -S -c \
      sourcefile.cpp -o sourcefile.llvm

Compile and link *sourcefile*`_gen.cpp`

    $ $LLVM_BUILD_DIR/bin/clang sourcefile_gen.cpp <other-compiler-flags>

Acknowledgments
---------------

Most of the initial work on this project was done by Girish Mururu, interning
at Intel during the summer of 2015. Additional work by Pablo Halpern
<pablo.g.halpern@intel.com>.

BSD LICENSE
===========

Copyright (C) 2015, Intel Corporation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of Intel Corporation nor the names of its
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.