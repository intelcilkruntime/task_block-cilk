/* task_block.t.cpp                  -*-C++-*-
 *
 *************************************************************************
 *
 *  Copyright (C) 2015, Intel Corporation
 *  All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *  
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *    * Neither the name of Intel Corporation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *  
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 *  OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 *  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 *  WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *  
 *************************************************************************/

#include "task_block.h"
#include <iostream>
#include <cstdlib>
#include <unistd.h>

using namespace std::experimental;

int g_n = 0;

void noop()
{
}

int fib(int n);

void spawn_fib2(parallel::task_block& tb, int n, int& ret);

void spawn_fib(parallel::task_block& tb, int n, int& ret)
{
    tb.run([&]{ ret = fib(n); });
}

void spawn_fib2(parallel::task_block& tb, int n, int& ret)
{
    tb.run([&]{ ret = fib(n); });
}

// Usage example
int fib(int n)
{
    if (0 == g_n)
        g_n = n;
    else if (g_n - 1 == n)
        sleep(1);

    if (n < 2) return n;

    typedef parallel::task_block& TBRef;
    int a, b;
    parallel::define_task_block([&](TBRef g) {
            noop();

            spawn_fib(g, n - 1, a);
            g.run([&]{ b = fib(n - 2); });
            // g.wait();
        });

    if (g_n == n)
        g_n = 0;
    return a + b;
}

namespace silly {
    struct parallelx {
        struct task_block {
            template <class F> void run(F&&) { }
            void wait() { }
        };
    };
} // end namespace silly

void foo()
{
    typedef silly::parallelx parallel;

    parallel::task_block xyz;
    xyz.run([&](parallel::task_block& tb) {
        tb.wait();
        });
}

#ifndef TEST_ERROR_DETECTION
#   define TEST_ERROR_DETECTION 0
#endif

#if TEST_ERROR_DETECTION
char garbage[sizeof(parallel::task_block)] = { };
parallel::task_block& fake_tb = *reinterpret_cast<parallel::task_block*>(garbage);
#endif

int main(int argc, char* argv[])
{
    int n = 30;
    if (argc > 1)
        n = std::atoi(argv[1]);

#if TEST_ERROR_DETECTION
    if (n < 0) {
        fake_tb.run(noop);  // Error: There is no active task block
        parallel::define_task_block([&](parallel::task_block& tb) {
                parallel::define_task_block([&](parallel::task_block& tb2) {
                        tb.run(noop);   // Error `tb` is not innermost `task_block`
                        tb2.run([&]{
                                tb2.wait(); // Error: `tb2` is not active within `run`
                            });
                        tb2.run([&]{
                                parallel::define_task_block([&](parallel::task_block& tb2){
                                        tb2.wait(); // OK
                                    });
                            });
                        tb2.run(noop);  // OK
                        tb.wait();      // Error `tb` is not innermost `task_block`
                        int a;
                        spawn_fib(tb, n, a); // Error `tb` is not innermost `task_block`
                        parallel::task_block& tb3 = tb2;
                        tb3.run(noop);  // False error: tb3 not known to be active
                    });
            });
    }
#endif

    std::cout << "fib(" << n << ") == " << fib(n) << std::endl;
}

/* End task_block.t.cpp */
