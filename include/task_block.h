/* task_block.h                  -*-C++-*-
 *
 *************************************************************************
 *
 *  Copyright (C) 2015, Intel Corporation
 *  All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *  
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *    * Neither the name of Intel Corporation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *  
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 *  OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 *  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 *  WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *  
 *************************************************************************/

/**
 * @file task_block.h
 *
 * @brief
 */

#ifndef INCLUDED_TASK_BLOCK_DOT_H
#define INCLUDED_TASK_BLOCK_DOT_H

#include <internal/cilk_fake.h>
#include <exception>
#include <utility>
#include <cstdint>

#include <alloca.h>

namespace std {
namespace experimental {
namespace parallel {
}
}
}

// Abbreviation of namespace
namespace __std_par = std::experimental::parallel;
    
namespace std {
namespace experimental {
namespace parallel {
inline namespace v1 {
    
class exception_list
{
};

} // close namespace v1

inline namespace v2 {

class task_canceled_exception : public exception {
public:
    virtual const char* what() const noexcept { return "task canceled"; }
};

template <typename F> void define_task_block(F&& f);
template <typename F> void define_task_block_restore_thread(F&& f);

class __tb_stack_tracker;

class task_block {
private:
    __cilkrts_stack_frame m_sf;
    exception_list        m_exceptions;
    char*                 m_resume_stack;

    template <typename F> friend void define_task_block(F&& f);
    template <typename F> friend void define_task_block_restore_thread(F&& f);
    friend class __tb_stack_tracker;

    task_block() : m_resume_stack(nullptr) { }
    ~task_block() { }

public:
    task_block(const task_block&) = delete;
    task_block& operator=(const task_block&) = delete;
    task_block* operator&() const = delete;

    template <typename F>
    void run(F&& f) __attribute__((noinline));

    template <typename R>
    void run(R (&f)()) { run(&f); }

    void wait();

    // Back door for compiler use
    __cilkrts_stack_frame& __get_sf() { return m_sf; }
    
    char* __resume_stack() const { return m_resume_stack; }
};

class __tb_stack_tracker
{
    task_block&         m_tb;
    char*               m_resume_stack;

//    static std::ptrdiff_t m_adjustment;
    
public:
    static char* sp() __attribute__((noinline));
//    static void init();

    __tb_stack_tracker(task_block& tb) : m_tb(tb), m_resume_stack(nullptr) {
    }

    ~__tb_stack_tracker() {
        m_tb.m_resume_stack = m_resume_stack;
    }

    void set_resume_stack(char* p = sp()) { m_resume_stack = p; }

    char* resume_stack() const {
        return m_resume_stack;
    }
};

// std::ptrdiff_t __tb_stack_tracker::m_adjustment = 0;

char* __tb_stack_tracker::sp()
{
    char v = 0;
    // Disguise pointer to avoid warning about returning local address
    std::intptr_t ip = (std::intptr_t) &v; 
    return (char*) ip;
}

// void __tb_stack_tracker::init()
// {
//     task_block dummy;
//     {
//         __tb_stack_tracker temp(&dummy);
//         temp.checkpoint();
//     }
//     m_adjustment = -dummy.__stack_offset();
// }

// For each function that is called with a `task_block&` argument,
// a boolean keeps track of whether a steal occured within that function.
// This flag is *not* set when a steal occurs in a child function.  The flag
// is reset on sync.
#define TB_CALLEE_PROLOG(tb)                             \
    __std_par::__tb_stack_tracker __stack_tracker(tb);

#define TB_RUN_WRAPPER(tb, RUN_EXPR) do {                                \
    CILK_FAKE_FORCE_FRAME_PTR((tb).__get_sf());                          \
    CILK_FAKE_DEFERRED_ENTER_FRAME((tb).__get_sf());                     \
    CILK_FAKE_SAVE_FP((tb).__get_sf());                                  \
    __cilkrts_worker* __tb_worker_before_spawn = (tb).__get_sf().worker; \
    if (__builtin_expect(! CILK_SETJMP((tb).__get_sf().ctx), 1)) {       \
        RUN_EXPR;                                                        \
    }                                                                    \
    if (__tb_worker_before_spawn != (tb).__get_sf().worker)              \
        __stack_tracker.set_resume_stack();                              \
} while (false)

#define TB_WAIT_WRAPPER(tb, WAIT_EXPR) do {                               \
    __cilkrts_stack_frame& __sf = tb.__get_sf();                          \
    if (__builtin_expect(__sf.flags & CILK_FRAME_UNSYNCHED, 0))      {    \
        __sf.parent_pedigree = __sf.worker->pedigree;                     \
        CILK_FAKE_SAVE_FP(__sf);                                          \
        if (! CILK_SETJMP(__sf.ctx))                                      \
            WAIT_EXPR;                                                    \
    }                                                                     \
    ++__sf.worker->pedigree.rank;                                         \
    __stack_tracker.set_resume_stack(nullptr);                            \
} while (0)

static void* __dummy_ptr;

#define TB_CALL_WRAPPER(tb, CALL_EXPR) do {                                   \
    CALL_EXPR;                                                                \
    if (tb.__resume_stack()) {                                                \
        __std_par::__dummy_ptr =                                              \
            alloca(__std_par::__tb_stack_tracker::sp() - tb.__resume_stack());\
        __stack_tracker.set_resume_stack(tb.__resume_stack());                \
    }                                                                         \
} while (0)

template <typename F>
void define_task_block(F&& f)
{
    task_block tb;
    // Dummy variable because there is no __stack_tracker in this function.
    struct { static void set_resume_stack(char*) { } } __stack_tracker;
    CILK_FAKE_FORCE_FRAME_PTR(tb.m_sf);
    CILK_FAKE_INITIAL_ENTER_FRAME(tb.m_sf);

    try {
        TB_CALL_WRAPPER(tb, f(tb));
    }
    catch (task_canceled_exception) {
    }
    catch (...) {
        // Add exception to tb.m_exceptions
    }

    try {
        TB_WAIT_WRAPPER(tb, tb.wait());
    }
    catch (task_canceled_exception) {
    }
    catch (...) {
        // Add exception to tb.m_exceptions
    }

    // Equivalent to CILK_FAKE_CLEANUP_FRAME(tb.m_sf);
    // Frame is in sync now.  No need to sync again.
    if (tb.m_sf.worker)
    {
        CILK_FAKE_SYNC_IMP(tb.m_sf);
        CILK_FAKE_POP_FRAME(tb.m_sf);
        if (tb.m_sf.flags != CILK_FAKE_VERSION_FLAG)
            __cilkrts_leave_frame(&tb.m_sf);
    }

    // if (tb.m_exceptions is not empty)
    //     throw tb.m_exceptions;
}

// Has the effect of a spawn helper.
// Must be called within TB_RUN_WRAPPER.
template <typename F>
void task_block::run(F&& f)
{
    F f_copy(std::forward<F>(f));

    CILK_FAKE_SPAWN_HELPER_PROLOG(m_sf);
    __cilk_fake_detach(&__cilk_sf);  // not m_sf, which is the parent
    // TBD: Exception handling goes here
    f_copy();
    CILK_FAKE_SPAWN_HELPER_EPILOG();
}

// Must be called within TB_WAIT_WRAPPER
void task_block::wait()
{
    __cilkrts_sync(&m_sf);
}

} // close namespace v2
} // close namespace parallel
} // close namespace experimental
} // close namespace std

#endif // ! defined(INCLUDED_TASK_BLOCK_DOT_H)
