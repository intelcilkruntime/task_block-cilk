/* PluginTaskBlock.cpp                  -*-C++-*-
 *
 *************************************************************************
 *
 *  Copyright (C) 2015, Intel Corporation
 *  All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *  
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in
 *      the documentation and/or other materials provided with the
 *      distribution.
 *    * Neither the name of Intel Corporation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *  
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 *  OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 *  AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 *  WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *  
 *************************************************************************/

#include "clang/AST/AST.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Frontend/FrontendPluginRegistry.h"
#include "llvm/Support/raw_ostream.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/AST/ParentMap.h"


#include <memory>
#include <iostream>
#include <sstream>
#include <queue>
#include <unistd.h>
using namespace clang;
using namespace llvm;
Rewriter rewriter;


class RTaskBlockVisitor : public RecursiveASTVisitor<RTaskBlockVisitor>
{
private:
    typedef RecursiveASTVisitor<RTaskBlockVisitor> Base;

    static constexpr NamedDecl *InactiveTb = (NamedDecl *) -1; ///< Special pointer value for inactive task block

    ASTContext        *Context;        ///< Context used by rewriter
    DiagnosticsEngine  Diag;           ///< Used for printing diagnostics
    NamedDecl         *InnerTb;        ///< Innermost task block declaration within AST traversal
    Expr              *RunTarget;      ///< The invocable target of the most recent `task_block::run` call
    const Type        *TaskBlockTypeP; ///< Pointer to the type node for `parallel::task_block` declaration
    bool               Error;          ///< True if an error was detected

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief Detect if a type matches `std::experimental::parallel::v2::task_block`
    /// \param QT A qualified type
    /// \return true if @param(QT) has type matching previously-declared `task_block`
    ////////////////////////////////////////////////////////////////////////////////////////////////
    bool IsTaskBlock(QualType QT);

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief Detect if a type is a n lvalue reference to `std::experimental::parallel::v2::task_block`
    /// \param QT A qualified type
    /// \return true if @param(QT) is a reference to a type matching previously-declared `task_block`
    ////////////////////////////////////////////////////////////////////////////////////////////////
    bool IsTaskBlockLValueRef(QualType QT);

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief Insert the `TB_CALLEE_PROLOG` macro before the first statement of a compound statement.
    /// \param CS     Pointer to a 'CompoundStmt` AST node
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void InsertProlog(CompoundStmt *CS);

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // \brief Instrument a function if it has a `task_block&` argument
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void InstrumentFunctionWithTaskBlock(FunctionDecl *FD);

    ///////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief Wrap an expression in a wrapper macro
    /// \param E         AST node for expression to be wrapped
    /// \param MacroName Name of macro wrapper
    /// This function takes an expression like `tb.wait()` and converts it into
    /// `TB_WAIT_WRAPPER(tb,tb.wait())`, where `E` represents the expression `tb.wait()`
    /// and `MacroName` is the string "TB_WAIT_WRAPPER".  The first argument to the wrapper
    /// is always the name of the innermost task block name, as found in `InnerTb`.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    void WrapExpressionWithMacro(Expr *E, std::string MacroName);
            
    ///////////////////////////////////////////////////////////////////////////////////////////////
    /// \brief Check the WAIT_FOR_DEBUGGER env variable
    /// \return true if WAIT_FOR_DEBUGGER env variable exists and is set to a non-zero value.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    static bool DebuggerEnv();

public:
    ///////////////////////////////
    /// \brief Constructor
    ///////////////////////////////
    explicit RTaskBlockVisitor(CompilerInstance &CI);

    ///////////////////////////////
    /// \brief destructor
    ///////////////////////////////
    ~RTaskBlockVisitor();

    ///////////////////////////////
    /// \brief Return the error flag
    /// \return true if an error was detected; else false
    ///////////////////////////////
    bool hasError() const { return Error; }

    //////////////////////////////////////////////////////
    /// \brief Intercept traversal of a declaration
    /// \param D  AST node for declaration being traversed
    /// Override the normal traversal of a declaration node in
    /// order to keep a stack of `task_block` nestings. Calls
    /// the base class's `TraverseDecl` function to do the
    /// real work.
    //////////////////////////////////////////////////////
    bool TraverseDecl(Decl *D);

    //////////////////////////////////////////////////////
    /// \brief Intercept traversal of a statement or expression
    /// \param D  AST node for statement or expression being traversed
    /// Override the normal traversal of a statement node in
    /// order to keep a stack of `task_block` nestings. Calls
    /// the base class's `TraverseStmt` function to do the
    /// real work.
    //////////////////////////////////////////////////////
    bool TraverseStmt(Stmt *S);

    //////////////////////////////
    /// \brief Visit a declaration AST node
    /// \param D  The declaration being visited
    /// \return always true
    /// When visiting a declaration, we are interested in two kinds of declarations:
    ///   o Function definitions having a `task_block&` argument
    ///   o Class definition for `std::experimental::parallel::v2::task_block`
    /////////////////////////////////
    bool VisitDecl(Decl *D);

    /////////////////////////////////
    /// \brief Visit a statement or expression AST node
    /// \param S The statement or expression being visited
    /// \return always true
    /// When visiting a statement or expression, we are interested in the
    /// following types of expression:
    ///   o A lambda expression taking an argument of type `task_block&`
    ///   o A member call to `task_block::run` or `task_block::wait`
    ///   o A call to a function taking an argument of type `task_block&`
    ///   o The use of an expression of type `task_block&`
    //////////////////////////////////
    bool VisitStmt(Stmt *S);

};

class RTaskBlockConsumer : public ASTConsumer{
private:
    RTaskBlockVisitor Visitor;
public:
    explicit RTaskBlockConsumer(CompilerInstance &CI) : Visitor(CI) { }

    virtual void HandleTranslationUnit(ASTContext &Context)
    {
        Visitor.TraverseDecl(Context.getTranslationUnitDecl());
        if (Visitor.hasError())
            return;

        // Get main file name
        SourceManager& srcmgr = Context.getSourceManager();
        FileID fid = srcmgr.getMainFileID();
        const FileEntry* fentry = srcmgr.getFileEntryForID(fid);
        assert(fentry);
        const char* fname = fentry->getName();
        assert(fname);

        // rewriter.overwriteChangedFiles();

        // Generate output file name from main source file name.
        std::string outName(fname);
        auto dot = outName.rfind('.');
        if (dot == std::string::npos)
            dot = outName.length();
        outName.insert(dot, "_gen");

        llvm::errs() << "Generated file name = " << outName << "\n";
        std::string OutErrorInfo;
        llvm::raw_fd_ostream outFile(outName.c_str(), OutErrorInfo);

        if (! OutErrorInfo.empty()) {
            llvm::errs() << "Cannot open " << outName << " for writing\n"
                         << "  " << OutErrorInfo << "\n";
            return;
        }

        // Line directive at top of generated file causes error messages to
        // use source file name during recompilation, instead of using
        // generated file name.
        outFile << "#line 1 \"" << fname << "\"\n";

        // Now output rewritten source code
        const RewriteBuffer *rewriteBuf = rewriter.getRewriteBufferFor(fid);
        outFile << std::string(rewriteBuf->begin(), rewriteBuf->end());
        outFile.close();
    }

};

class RTaskBlockAction: public PluginASTAction{

protected:
    ASTConsumer* CreateASTConsumer(CompilerInstance &CI, StringRef InFile)
    {
        RTaskBlockConsumer *Consumer = new RTaskBlockConsumer(CI);
        return Consumer;
    }

    bool ParseArgs(const CompilerInstance &CI, const std::vector<std::string> &args)
    {
        return true;
    }

};


static FrontendPluginRegistry::Add<RTaskBlockAction>
X("TaskBlock","implement task_block technical specification");


/////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     IMPLEMENTATION
/////////////////////////////////////////////////////////////////////////////////////////////////////////

inline bool RTaskBlockVisitor::IsTaskBlock(QualType QT)
{
    if (! TaskBlockTypeP)
        return false;  // Haven't seen a declaration of `task_block`, so nothing to match.

    return TaskBlockTypeP == QT->getUnqualifiedDesugaredType();
}

bool RTaskBlockVisitor::IsTaskBlockLValueRef(QualType QT)
{
    return QT->isLValueReferenceType() && IsTaskBlock(QT.getNonReferenceType());
}

void RTaskBlockVisitor::InsertProlog(CompoundStmt *CS)
{
    assert(InnerTb);

    if (! CS->size())
        return;  // Do not insert prolog into empty compound statement

    SourceLocation SSt = begin(CS->children())->getLocStart(); // Loc of first child
    std::string Insert = "TB_CALLEE_PROLOG("+ InnerTb->getNameAsString() +"); ";   // No newline; preserve line numbers
    rewriter.InsertText(SSt,Insert,true, true);
}

void RTaskBlockVisitor::InstrumentFunctionWithTaskBlock(FunctionDecl *FD)
{
    if (! FD->doesThisDeclarationHaveABody())
        return;  // Skip declarations without bodies even if the body has been seen elsewhere.

    CompoundStmt *CS = dyn_cast<CompoundStmt>(FD->getBody());
    if (! CS)
        return;  // No CompoundStmt body.  Do nothing.

    // Traverse the argument list looking for one of type `parallel::task_block&`
    ParmVarDecl *tb_arg = nullptr;
    for(auto paramI = FD->param_begin(), paramE = FD->param_end(); paramI != paramE; ++paramI)
    {
        ParmVarDecl *PV = *paramI;
        if (IsTaskBlockLValueRef(PV->getType())) {
            tb_arg = PV;
            break;
        }
    }

    if (! tb_arg)
        return; // No `task_block&` argument.  Do nothing.

    InnerTb = tb_arg;  // Remember this name in a member variable
    InsertProlog(CS);
}

void RTaskBlockVisitor::WrapExpressionWithMacro(Expr *E, std::string MacroName)
{
    SourceLocation SSt = E->getLocStart();
    MacroName += "(" + InnerTb->getNameAsString() + ",";
    rewriter.InsertText(SSt, MacroName, true, true);
    SourceLocation SSe = E->getLocEnd();
    rewriter.InsertTextAfterToken(SSe,")");
}

bool RTaskBlockVisitor::DebuggerEnv() {
    const char* envstr = getenv("WAIT_FOR_DEBUGGER");
    if (envstr && '0' <= envstr[0] && envstr[0] <= '9')
        return std::atoi(envstr);
    else
        return false;
}

RTaskBlockVisitor::RTaskBlockVisitor(CompilerInstance &CI)
    : Context(&CI.getASTContext())
    , Diag(CI.getDiagnostics())
    , InnerTb(nullptr)
    , RunTarget(nullptr)
    , TaskBlockTypeP(nullptr)
    , Error(false)
{
    // unsigned DiagId = Diag.getCustomDiagID(DiagnosticsEngine::Warning,"Visitor pid: %0");
    // Diag.Report(DiagId) << getpid();

    static volatile bool WaitForDebugger = DebuggerEnv();
    if (WaitForDebugger) {
        std::cerr << "Plugin process " << getpid() << " waiting for debugger" << std::endl;
        while (WaitForDebugger)
            sleep(1);
    }
    rewriter.setSourceMgr(Context->getSourceManager(),Context->getLangOpts());
}

RTaskBlockVisitor::~RTaskBlockVisitor()
{
}

inline bool RTaskBlockVisitor::TraverseDecl(Decl *D)
{
    if (D && dyn_cast<FunctionDecl>(D)) {
        // Push-pop InnerTb for function declarations
        NamedDecl *OuterTb = InnerTb;     // Push InnerTb
        bool ret = Base::TraverseDecl(D); // Call baseclass function
        InnerTb = OuterTb;                // Pop InnerTb
        return ret;
    }
    else
        return Base::TraverseDecl(D); // Call baseclass function
}

inline bool RTaskBlockVisitor::TraverseStmt(Stmt *S)
{
    if (S && dyn_cast<LambdaExpr>(S)) {
        // Push-pop InnerTb for lambda expressions
        NamedDecl *OuterTb = InnerTb;     // Push InnerTb
        if (S == RunTarget) {
            // The lambda expression is the invocable called by `task_block::run`.
            // All outside task blocks are inactive within the lambda definition.
            InnerTb = InactiveTb;
            RunTarget = nullptr;
        }
        bool ret = Base::TraverseStmt(S); // Call baseclass function
        InnerTb = OuterTb;                // Pop InnerTb
        return ret;
    }
    else
        return Base::TraverseStmt(S); // Call baseclass function
}

bool RTaskBlockVisitor::VisitDecl(Decl *D)
{
    // If I could figure out what the enumerations for Kind are, I could probably make
    // this code more efficient by using a switch statement:
    // switch (D->getKind()) {
    //     case Decl::Function:

    if (FunctionDecl *Fd = dyn_cast<FunctionDecl>(D)) {
        // Function declaration.  Get the parent map for the Function visited required in VisitStmt.
        if(Fd->hasBody())
            InstrumentFunctionWithTaskBlock(Fd);
    }
    else if (CXXRecordDecl *Cd = dyn_cast<CXXRecordDecl>(D)) {
        // Class, struct, or enum declaration.  If this is a class declaration, check to see if
        // the class being declared is "std::experimental::parallel::v2::task_block".
        // If so, save the type being declared so that we can identify variables in the future
        // as being of type `task_block`.
        // Note: `getName()` is probably much cheaper than `getQualifiedNameAsString()`, hence
        // we check it first.  Classes named "task_block" should be rare, so the expensive call
        // will be made rarely.
        if (Cd->isClass() && Cd->getName() == "task_block" &&
            Cd->getQualifiedNameAsString() == "std::experimental::parallel::v2::task_block") {

            // We found the declaration we're looking for. Save a pointer to the type being declared.
            TaskBlockTypeP = Cd->getTypeForDecl();

            // For debugging only:
            // unsigned DiagId = Diag.getCustomDiagID(DiagnosticsEngine::Warning,"Found task_block decl: %0");
            // Diag.Report(Cd->getLocStart(), DiagId) << (const void*) TaskBlockTypeP;
        }
    }

    return true;
}

bool RTaskBlockVisitor::VisitStmt(Stmt *S)
{
    if (LambdaExpr *LE = dyn_cast<LambdaExpr>(S)) {
        // Process a lambda expression like any other function.
        // If the lambda takes an argument of type `task_block&`, instrument it.
        InstrumentFunctionWithTaskBlock(LE->getCallOperator());
    }
    else if (CXXMemberCallExpr *Cmc = dyn_cast<CXXMemberCallExpr>(S)) {
        // Process a member call to `task_block::run` or `task_block::wait`.
        // Wrap the call in the appropriate `TB_RUN_WRAPPER` or `TB_WAIT_WRAPPER` macro.
        // In the case of `run`, store a pointer to the target invocable for later
        // error checking.
        Expr *ImplicitArg = Cmc->getImplicitObjectArgument();
        if (InnerTb && InnerTb != InactiveTb && IsTaskBlock(ImplicitArg->getType())) {
            if (MemberExpr *Me = dyn_cast<MemberExpr>(Cmc->getCallee())) {

                std::string MemberName = Me->getMemberDecl()->getNameAsString();
                if (MemberName == "run" && Cmc->getNumArgs() > 0) {
                    if (MaterializeTemporaryExpr *Mt = dyn_cast<MaterializeTemporaryExpr>(Cmc->getArgs()[0]))
                        RunTarget = Mt->GetTemporaryExpr();  // Remember the target invocable to be run
                    else
                        RunTarget = nullptr;
                    WrapExpressionWithMacro(Cmc, "TB_RUN_WRAPPER");
                }
                else if (MemberName == "wait")
                    WrapExpressionWithMacro(Cmc, "TB_WAIT_WRAPPER");
            }
        }
    }
    else if (CallExpr *CE = dyn_cast<CallExpr>(S)) {
        // If we have a call expression, see if the current `task_block` is being passed to the call.
        // If so, wrap the call with the `TB_CALL_WRAPPER` macro.
        // There is no need to check for use of task block as an argument if none has been passed to us.
        if (InnerTb) {
            for (auto ArgI = CE->arg_begin(), ArgE = CE->arg_end(); ArgI != ArgE; ++ArgI) {

                if (IsTaskBlock(ArgI->getType()))
                    // Function call passes a TB argument
                    WrapExpressionWithMacro(CE, "TB_CALL_WRAPPER");
            }
        }
    }
    else if (DeclRefExpr *DR = dyn_cast<DeclRefExpr>(S)) {
        // Look for invalid uses of a `task_block` reference.
        if (NamedDecl *ND = DR->getFoundDecl()) {
            if (ValueDecl *VD = dyn_cast<ValueDecl>(ND->getUnderlyingDecl())) {
                if (IsTaskBlockLValueRef(VD->getType())) {
                    // Found a use of an expression of type `task_block&`.
                    // If expression does not represent the active (innermost) task block,
                    // warn the user. There is the posibility of false positives, so
                    // changing the warning to an error would not be appropriate.

                    // Debugging only:
                    // unsigned DiagId = Diag.getCustomDiagID(DiagnosticsEngine::Warning,"Found task_block use: %0");
                    // Diag.Report(DR->getLocStart(), DiagId) << ND->getNameAsString();

                    if (InnerTb == InactiveTb) {
                        // If inner task_block is inactive, it means that we are incorrectly
                        // using a task_block immediately within a call to `task_block::run`.
                        unsigned DiagId = Diag.getCustomDiagID(DiagnosticsEngine::Warning,
                                                               "Captured task_block %0 is inactive within call to run()");
                        Diag.Report(DR->getLocStart(), DiagId) << ND->getNameAsString();
                        // Error = true;  // ?
                    }
                    else if (! InnerTb) {
                        // There is no active task block at all.
                        unsigned DiagId = Diag.getCustomDiagID(DiagnosticsEngine::Error,
                                                               "Task block %0 is inactive.  "
                                                               "There is no active (innermost) task block.");
                        Diag.Report(DR->getLocStart(), DiagId) << ND->getNameAsString();
                        Error = true;
                    }
                    else if (ND->getMostRecentDecl() != InnerTb) {
                        // If the `task_block` reference is not the innermost task block
                        // then it is probably invalid.
                        unsigned DiagId = Diag.getCustomDiagID(DiagnosticsEngine::Warning,
                                                               "Task block %0 may be inactive.  "
                                                               "The active (innermost) task block is %1.");
                        Diag.Report(DR->getLocStart(), DiagId) << ND->getNameAsString() << InnerTb->getNameAsString();
                        // Error = true;  // ?
                    }
                }
            }
        }
    }
    return true;
}
